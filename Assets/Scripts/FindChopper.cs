﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FindChopper : MonoBehaviour
{
    private CinemachineVirtualCamera cam;

    void Awake()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cam.Follow == null || cam.LookAt == null)
        {
            var chopper = FindObjectOfType<ChopperTiltZ>();
            if (chopper)
            {
                cam.Follow = chopper.transform;
                cam.LookAt = chopper.transform;
            }
            
        }

    }
}
