﻿using NgnPhysics;
using NgnUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UserEraser : Eraser
{
    [SerializeField] protected InputActionReference eraseInput;

    protected override void OnEnable()
    {
        base.OnEnable();
        eraseInput.action.Enable();
        eraseInput.action.performed += OnInput;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        eraseInput.action.performed -= OnInput;
        eraseInput.action.Disable();
    }

    void OnInput(InputAction.CallbackContext ctx)
    {
        Use();
    }
}
