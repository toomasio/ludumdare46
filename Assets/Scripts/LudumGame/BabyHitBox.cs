﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BabyHitBox : MonoBehaviour
{
    [SerializeField] private BodyPartsDamageable damageable;

    public virtual void Hit(Vector3 _hitPos)
    {
        damageable.Kill(_hitPos);
    }
}
