﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saveable : MonoBehaviour
{
    private CheckPointManager manager;

    private float enteredTime;

    void OnEnable()
    {
        manager = CheckPointManager.FindObjectOfType<CheckPointManager>();
        enteredTime = manager.GameTime;
    }

    public void CheckTime()
    {
        if (enteredTime > manager.GameTime)
            Destroy(gameObject);
    }
}
