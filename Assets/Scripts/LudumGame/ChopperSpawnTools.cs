﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnPhysics;
using NgnUtilities;
using UnityEngine.InputSystem;

[System.Serializable]
public class SpawnInput
{
    public InputActionReference input;
    public GameObject prefabToSpawn;
    private System.Action<GameObject> callback;

    public void Initialize(System.Action<GameObject> _spawnCallback)
    {
        var action = input.action;
        action.Enable();
        action.performed += OnInput;
        callback = _spawnCallback;
    }

    public void Disable()
    {
        input.action.Disable();
    }

    void OnInput(InputAction.CallbackContext ctx)
    {
        callback.Invoke(prefabToSpawn);
    }
}

public class ChopperSpawnTools : MonoBehaviour
{
    [SerializeField, ExpandableObject] private SphereCast2DData rayDownData;
    [SerializeField] private Transform rayDownPos;
    [SerializeField] private SpawnInput[] spawnInputs;

    private IPhysicsDetectable2D rayDownSystem;

    private void Awake()
    {
        rayDownSystem = rayDownData.CreateSystemInstance(rayDownPos);
    }

    private void OnEnable()
    {
        for (int i = 0; i < spawnInputs.Length; i++)
            spawnInputs[i].Initialize(InputSpawn);
    }

    private void OnDisable()
    {
        for (int i = 0; i < spawnInputs.Length; i++)
            spawnInputs[i].Disable();
    }

    private void FixedUpdate()
    {
        if (rayDownSystem == null) return;

        rayDownSystem.Direction = Vector3.down;
        rayDownSystem.Tick();
    }

    private void InputSpawn(GameObject _go)
    {
        if (!rayDownSystem.Detected) return;
        if (rayDownSystem.HitPosition == Vector2.zero) return;

        Instantiate(_go, rayDownSystem.HitPosition, Quaternion.identity);

    }

    private void OnDrawGizmos()
    {
        if (rayDownData == null || rayDownPos == null) return;
        if (rayDownSystem == null) rayDownSystem = rayDownData.CreateSystemInstance(rayDownPos);

        rayDownSystem.Direction = Vector3.down;
        if (!Application.isPlaying) rayDownSystem.Tick();
        rayDownSystem.DrawDetectable();
    }
}
