﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipCanvasSlot : MonoBehaviour
{
    public Image icon;

    private RectTransform trans;

    private void Awake()
    {
        trans = GetComponent<RectTransform>();
    }

    public void SetSelected(bool _selected)
    {
        trans.localScale = _selected ? Vector3.one * 1.1f : Vector3.one;
    }
}
