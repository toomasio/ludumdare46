﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyJumpBoard : Physics2DOnEnter
{
    [SerializeField] private float heightPower;
    [SerializeField] private float forwardPower;

    protected override void OnEnter(Collider2D col)
    {
        var rb = col.GetComponent<Rigidbody2D>();
        if (rb)
        {
            var force = col.transform.right * forwardPower;
            force.y = heightPower;
            rb.velocity = force;
        }
        base.OnEnter(col);
    }
}
