﻿using NgnPhysics;
using NgnUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eraser : Item
{
    [SerializeField, ExpandableObject] protected SphereCast2DData rayData;
    [SerializeField] protected Transform rayOrigin;
    [SerializeField] protected Vector2 direction;

    protected List<Selectable> curSelected = new List<Selectable>();
    protected IPhysicsDetectable2D raySystem;

    protected virtual void OnEnable()
    {
        raySystem = rayData.CreateSystemInstance(rayOrigin);
        raySystem.OnEnter += OnEnter;
        raySystem.OnExit += OnExit;
    }

    protected virtual void OnDisable()
    {
        raySystem.OnEnter -= OnEnter;
        raySystem.OnExit -= OnExit;
        for (int i = 0; i < curSelected.Count; i++)
        {
            curSelected[i].OnExit();
        }
        curSelected.Clear();
        
    }

    protected virtual void FixedUpdate()
    {
        TickRay();
    }

    void TickRay()
    {
        if (direction != default)
            raySystem.Direction = direction;
        raySystem.Tick();
    }

    void OnEnter(Collider2D col)
    {
        var sel = col.GetComponent<Selectable>();
        if (sel)
        {
            sel.OnEnter();
            curSelected.Add(sel);
        }
    }

    void OnExit(Collider2D col)
    {
        var sel = col.GetComponent<Selectable>();
        if (sel)
        {
            sel.OnExit();
            curSelected.Remove(sel);
        }
    }

    public override void Use()
    {
        EraseSelected();
    }

    protected virtual void EraseSelected()
    {
        if (!raySystem.Detected) return;
        for (int i = 0; i < curSelected.Count; i++)
        {
            if (curSelected[i] != null)
                Destroy(curSelected[i].gameObject);
        }
        curSelected.Clear();
    }

    protected void OnDrawGizmos()
    {
        if (rayData == null || rayOrigin == null) return;
        if (raySystem == null) raySystem = rayData.CreateSystemInstance(rayOrigin);

        if (!Application.isPlaying) TickRay();
        raySystem.DrawDetectable();
    }
}
