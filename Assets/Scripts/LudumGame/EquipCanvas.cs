﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipCanvas : MonoBehaviour
{
    [SerializeField] protected RectTransform slotParent;
    [SerializeField] protected EquipCanvasSlot slotPrefab;
    protected List<EquipCanvasSlot> equipSlots = new List<EquipCanvasSlot>();

    public virtual void SelectSlot(int _index)
    {
        for (int i = 0; i < equipSlots.Count; i++)
            equipSlots[i].SetSelected(i == _index);
    }

    public virtual void AddEquipSlot(Sprite _icon)
    {
        if (equipSlots.Count == 3) return;

        var slot = Instantiate(slotPrefab);
        slot.icon.sprite = _icon;
        slot.GetComponent<RectTransform>().SetParent(slotParent);
        equipSlots.Add(slot);
    }
    
}
