﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ngn/Game Event")]
public class GameEvent : ScriptableObject
{
    public event Action OnEvent;

    public void TriggerEvent()
    {
        OnEvent?.Invoke();
    }
}
