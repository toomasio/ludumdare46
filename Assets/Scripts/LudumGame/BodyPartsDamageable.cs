﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BodyPartsDamageable : Damageable
{
    [SerializeField] private BodyPart[] bodyParts;
    [SerializeField] private float explosionForce;
    [SerializeField] private Transform bodyCenter;

    public override void Kill(Vector3 _hitPos)
    {
        if (IsDead) return;
        base.Kill(_hitPos);

        var dir = (bodyCenter.position - _hitPos).normalized;
        for (int i = 0; i < bodyParts.Length; i++)
        {
            bodyParts[i].SpawnPart(dir, explosionForce);
        }
    }
}
