﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChunk : Physics2DOnEnter
{
    public Transform enterPos;
    public Transform exitPos;

    private CheckPointManager checkMan;
    private LevelChunkManager chunkMan;
    private bool entered;
    protected override void Awake()
    {
        base.Awake();
        checkMan = FindObjectOfType<CheckPointManager>();
        chunkMan = FindObjectOfType<LevelChunkManager>();
    }

    protected override void OnEnter(Collider2D col)
    {
        if (entered) return;
        entered = true;
        base.OnEnter(col);
        checkMan.SetCheckpoint(enterPos.position, enterPos.rotation);
        chunkMan.SpawnNextChunk();
    }
}
