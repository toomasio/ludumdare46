﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawnable : MonoBehaviour
{
    [SerializeField] private Vector2 startOffset;

    private CheckPointManager man;

    void Start()
    {
        man = FindObjectOfType<CheckPointManager>();
        man.Add(this);
        if (!man.Started) return;

        SetToCheckPointPosition();
    }

    public void SetToCheckPointPosition()
    {
        transform.position = man.LastCheckpointPos + startOffset;
        transform.rotation = man.LastCheckpointRot;
        var look = GetComponent<BabyLookController>();
        if (look)
            look.SetLook(man.LastCheckpointRot == Quaternion.identity ? Vector2.right : Vector2.left);
    }

}
