﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class MenuManager : MonoBehaviour
{

    [SerializeField] private InputActionReference pauseButton;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private UnityEvent onPauseEvents;
    [SerializeField] private UnityEvent onResumeEvents;
    public bool Started { get; private set; }
    public bool IsPaused { get; private set; }
    public void GameStarted() { Started = true; }

    private void Awake()
    {
        IsPaused = true;
    }

    private void OnEnable()
    {
        pauseButton.action.Enable();
        pauseButton.action.performed += OnPauseDown;
    }

    private void OnDisable()
    {
        pauseButton.action.Disable();
        pauseButton.action.performed -= OnPauseDown;
    }

    private void OnPauseDown(InputAction.CallbackContext ctx)
    {
        if (!Started) return;
        Pause();
    }

    public void Pause()
    {
        IsPaused = !IsPaused;

        Cursor.visible = IsPaused;
        Cursor.lockState = IsPaused ? CursorLockMode.None : CursorLockMode.Locked;
        Time.timeScale = IsPaused ? 0 : 1;
        pauseMenu.SetActive(IsPaused);

        if (IsPaused)
            onPauseEvents?.Invoke();
        else
            onResumeEvents?.Invoke();
    }

    public void Quit()
    {
        Application.Quit();
    }

}
