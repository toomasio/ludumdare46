﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damageable : MonoBehaviour
{
    [SerializeField] private UnityEvent onKillEvents;

    public bool IsDead { get; private set; }

    public virtual void Kill(Vector3 _hitPos)
    {
        if (IsDead) return;
        IsDead = true;
        onKillEvents?.Invoke();
        Destroy(gameObject);
    }
}
