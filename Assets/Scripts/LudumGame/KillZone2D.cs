﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnPhysics;
using NgnUtilities;
using UnityEngine.Events;

public class KillZone2D : MonoBehaviour
{
    [SerializeField, ExpandableObject] protected OverlapBox2DData detectData;
    [SerializeField] protected Transform detectPos;
    [SerializeField] protected UnityEvent onKillEvents;

    protected IPhysicsDetectable2D detectSystem;

    protected virtual void OnEnable()
    {
        detectSystem = detectData.CreateSystemInstance(detectPos);
        detectSystem.OnEnter += Kill;
    }

    protected virtual void OnDisable()
    {
        detectSystem.OnEnter -= Kill;
    }

    protected virtual void FixedUpdate()
    {
        detectSystem.Tick();
    }

    protected virtual void Kill(Collider2D col)
    {
        onKillEvents?.Invoke();
        var babyDam = col.GetComponent<BabyHitBox>();
        if (babyDam != null)
        {
            babyDam.Hit(detectSystem.HitPosition);
            return;
        }
            
        var damage = col.GetComponent<Damageable>();
        if (damage != null)
        {
            damage.Kill(detectSystem.HitPosition);
            return;
        }

        var damageSelf = GetComponent<Damageable>();
        if (damageSelf != null)
        {
            damageSelf.Kill(detectSystem.HitPosition);
        }
    }

    protected virtual void OnDrawGizmos()
    {
        if (detectData == null || detectPos == null) return;
        if (detectSystem == null) detectSystem = detectData.CreateSystemInstance(detectPos);

        Gizmos.color = Color.red;
        detectSystem.DrawDetectable();
    }
}
