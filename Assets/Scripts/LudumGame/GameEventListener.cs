﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    [SerializeField] private GameEvent gameEvent;
    [SerializeField] private UnityEvent onEventTriggered;

    void OnEnable()
    {
        gameEvent.OnEvent += OnEventTriggered;
    }

    void OnDisable()
    {
        gameEvent.OnEvent -= OnEventTriggered;
    }

    protected virtual  void OnEventTriggered()
    {
        onEventTriggered?.Invoke();
    }
}
