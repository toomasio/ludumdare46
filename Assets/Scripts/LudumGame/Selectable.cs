﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    [SerializeField] private GameObject selectedPrefab;
    private GameObject curSelected;

    private void OnDisable()
    {
        if (curSelected)
            Destroy(curSelected);
    }

    public void OnEnter()
    {
        if (!curSelected)
            curSelected = Instantiate(selectedPrefab, transform.position, transform.rotation);
    }

    public void OnExit()
    {
        if (curSelected)
            Destroy(curSelected);
    }
}
