﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChopperTiltZ : MonoBehaviour
{
    [SerializeField] private float leanSpeed;
    [SerializeField] private float leanAmount;

    private Vector3 diff;
    private Vector3 dir;
    private Vector3 lastPos;
    private float tarZ;
    private Quaternion tarRot;
    private Quaternion smoothRot;

    private void OnEnable()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        lastPos = transform.position;
    }

    private void FixedUpdate()
    {
        diff = transform.position - lastPos;
        dir = diff.normalized;

        if (dir.x < 0) tarZ = leanAmount;
        else if (dir.x > 0) tarZ = -leanAmount;
        else tarZ = 0;

        tarRot = Quaternion.Euler(0,0, tarZ);
        smoothRot = Quaternion.Slerp(transform.rotation, tarRot, leanSpeed * Time.deltaTime);
        transform.rotation = smoothRot;


        lastPos = transform.position;    
    }
}
