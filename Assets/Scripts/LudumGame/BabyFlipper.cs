﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnUtilities;
using NgnPhysics;
using UnityEngine.Events;

public class BabyFlipper : Physics2DOnEnter
{
    private BabyLookController lookController;

    protected override void Awake()
    {
        base.Awake();
        lookController = GetComponent<BabyLookController>();
    }

    protected override void OnEnter(Collider2D col)
    {
        lookController.Flip();
        base.OnEnter(col);
    }
}
