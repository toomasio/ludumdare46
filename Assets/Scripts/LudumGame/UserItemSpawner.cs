﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NgnPhysics;
using UnityEngine.InputSystem;

public class UserItemSpawner : ItemSpawner
{
    [SerializeField] protected InputActionReference spawnInput;

    protected override void OnEnable()
    {
        base.OnEnable();
        spawnInput.action.Enable();
        spawnInput.action.performed += OnInput;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        spawnInput.action.performed -= OnInput;
        spawnInput.action.Disable();
    }

    void OnInput(InputAction.CallbackContext ctx)
    {
        Use();
    }
}
