﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnControllers;

public class BabyMoveController : MoveController
{
    Vector2 dir;
    float curFallTimer;
    Vector2 lastPos;

    protected override void Awake()
    {
        base.Awake();
        lastPos = transform.position;
    }

    protected override void FixedUpdate()
    {
        moveStates[curIndex].MoveInput = Vector3.right;
        base.FixedUpdate();

        dir = ((Vector2)transform.position - lastPos).normalized;


        //detect fall hack
        if (dir.y < -0.3f)
        {
            curFallTimer += Time.deltaTime;
            if (curFallTimer > 2)
            {
                GetComponent<Damageable>().Kill(transform.TransformPoint(Vector3.down));
            }
        }
        else
            curFallTimer = 0;

        lastPos = transform.position;
    }
}
