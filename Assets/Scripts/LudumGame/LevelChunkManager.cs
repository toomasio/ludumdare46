﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelChunkManager : MonoBehaviour
{
    [SerializeField] private LevelChunk startChunk;
    [SerializeField] private LevelChunk[] chunks;
    [SerializeField] private int loadChunkAhead = 5;
    [SerializeField] private int maxChunkBehind = 5;

    private LevelChunk curChunk;

    private List<LevelChunk> spawnedChunks = new List<LevelChunk>();

    private int loadAheadCount;
    private int behindCount;

    void Awake()
    {
        if (startChunk)
            curChunk = startChunk;

        loadAheadCount = loadChunkAhead;
    }

    public void SpawnNextChunk()
    {
        int rand = Random.Range(0, chunks.Length);

        for (int i = 0; i < loadAheadCount; i++)
        {
            var pos = curChunk.exitPos.position;
            pos.z = 0;
            curChunk = Instantiate(chunks[rand], pos, Quaternion.identity);
            spawnedChunks.Insert(0, curChunk);  
        }
        loadAheadCount = 1;
        behindCount++;
        if (behindCount >= maxChunkBehind)
        {
            var last = spawnedChunks[spawnedChunks.Count - 1].gameObject;
            Destroy(last.gameObject);
            spawnedChunks.RemoveAt(spawnedChunks.Count - 1);
            behindCount = 0;
        }

    }
  
}
