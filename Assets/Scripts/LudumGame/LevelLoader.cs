﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private Slider loadingBar = null;
    [SerializeField] private CanvasGroup canvasGroup = null;
    [SerializeField] private float fadeTime = 3;
    private AsyncOperation levelToLoad;
    private bool loading;
    private float timer;
    private float fade;

    void OnEnable()
    {
        DontDestroyOnLoad(gameObject);
        canvasGroup.alpha = 0;
    }

    public void LoadLevel(string _name)
    {
        levelToLoad = SceneManager.LoadSceneAsync(_name);
        levelToLoad.allowSceneActivation = false;
        loading = true;
    }

    void Update()
    {
        if (!loading) return;

        canvasGroup.alpha = 1;
        loadingBar.value = levelToLoad.progress;

        if (levelToLoad.progress == 0.9f)
        {
            levelToLoad.allowSceneActivation = true;
        }

        timer += Time.deltaTime;
        var perc = timer < fadeTime ? timer / fadeTime : 1;
        fade = Mathf.Lerp(1, 0, perc);
        canvasGroup.alpha = fade;
        if (perc == 1)
            Destroy(gameObject);

    }
}
