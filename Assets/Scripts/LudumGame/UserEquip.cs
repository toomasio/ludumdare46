﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UserEquip : MonoBehaviour
{
    [SerializeField] protected InputActionReference nextInput;
    [SerializeField] protected InputActionReference previousInput;
    [SerializeField] protected EquipCanvas canvas;
    [SerializeField] protected Item[] items;
    [SerializeField] protected Transform equipPos;

    protected int curItemInd;
    protected Item curItem;

    protected virtual void Awake()
    {
        canvas = FindObjectOfType<EquipCanvas>();

        for (int i = 0; i < items.Length; i++)
        {
            canvas.AddEquipSlot(items[i].icon);
        }

        SwitchItem(curItemInd);
    }

    private void OnEnable()
    {
        nextInput.action.Enable();
        previousInput.action.Enable();
        nextInput.action.performed += OnNextInput;
        previousInput.action.performed += OnPrevInput;
    }

    private void OnDisable()
    {
        nextInput.action.performed -= OnNextInput;
        previousInput.action.performed -= OnPrevInput;
        nextInput.action.Disable();
        previousInput.action.Disable();
    }

    void OnNextInput(InputAction.CallbackContext ctx)
    {
        NextItem(); 
    }

    void OnPrevInput(InputAction.CallbackContext ctx)
    {
        PreviousItem();
    }

    protected virtual void PreviousItem()
    {
        if (curItemInd == 0)
            curItemInd = items.Length - 1;
        else
            curItemInd--;
        SwitchItem(curItemInd);
    }

    protected virtual void NextItem()
    {
        if (curItemInd == items.Length - 1)
            curItemInd = 0;
        else
            curItemInd++;
        SwitchItem(curItemInd);
    }

    protected virtual void SwitchItem(int _index)
    {
        if (curItem)
            Destroy(curItem.gameObject);

        curItem = Instantiate(items[_index]);
        curItem.transform.position = equipPos.position;
        curItem.transform.rotation = equipPos.rotation;
        curItem.transform.SetParent(equipPos);

        if (canvas)
            canvas.SelectSlot(_index);
    }
}
