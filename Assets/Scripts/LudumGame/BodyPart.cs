﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    [SerializeField] private GameObject physicsBodyPart;

    private Rigidbody2D spawn;

    public void SpawnPart(Vector3 _direction, float _force)
    {
        spawn = Instantiate(physicsBodyPart, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity)
            .GetComponent<Rigidbody2D>();
        spawn.AddForce(_direction * _force, ForceMode2D.Impulse);

        Destroy(gameObject);
    }
}
