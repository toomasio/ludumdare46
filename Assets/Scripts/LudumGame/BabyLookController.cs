﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnControllers;

public class BabyLookController : LookController
{
    public bool flip;

    protected override void InitializeProfile()
    {
        base.InitializeProfile();
        lookStates[curIndex].LookInput = Vector2.right;
    }

    protected override void Update()
    {
        base.Update();
        if (flip)
        {
            Flip();
            flip = false;
        }
    }

    public void SetLook(Vector2 _input)
    {
        lookStates[curIndex].LookInput = _input;
    }

    public void Flip()
    {
        lookStates[curIndex].LookInput = -lookStates[curIndex].LookInput;
    }
}
