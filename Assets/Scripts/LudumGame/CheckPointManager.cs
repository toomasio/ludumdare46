﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class CheckPointManager : MonoBehaviour
{
    [SerializeField] private Transform startPos;
    [SerializeField] private UnityEvent onCheckPointEvents;
    [SerializeField] private UnityEvent onResetEvents;
        private List<Respawnable> respawnablesSceneObjects = new List<Respawnable>();
    [SerializeField] private Respawnable[] respawnablesPrefabs;

    public bool Started { get; private set; }
    public void OnStart() { Started = true; }
    public float GameTime { get; private set; }
    private float curCheckPointTime;

    public Vector2 LastCheckpointPos { get; private set; }
    public Quaternion LastCheckpointRot { get; private set; }

    private bool respawning;
    private float timer;

    void Awake()
    {
        SetCheckpoint(startPos.position, startPos.rotation);
    }

    void Update()
    {
        GameTime += Time.deltaTime;
        if (!respawning) return;

        //dont judge me for this bandaid...I know :/
        timer += Time.deltaTime;
        if (timer > 3)
        {
            for (int i = 0; i < respawnablesSceneObjects.Count; i++)
            {
                if (respawnablesSceneObjects[i] != null)
                    respawnablesSceneObjects[i].SetToCheckPointPosition();
                else
                    respawnablesSceneObjects.RemoveAt(i);
            }

            bool spawned = false;
            for (int i = 0; i < respawnablesPrefabs.Length; i++)
            {

                for (int j = 0; j < respawnablesSceneObjects.Count; j++)
                {
                    if (respawnablesSceneObjects[j] != null)
                    {
                        if (respawnablesSceneObjects[j].name.Replace("(Clone)", "") != respawnablesPrefabs[i].name)
                        {
                            Instantiate(respawnablesPrefabs[i]);
                            spawned = true;
                        }
                            
                    }
                }
                
            }

            //spawn both if both are dead
            if (!spawned)
            {
                for (int i = 0; i < respawnablesPrefabs.Length; i++)
                {
                    Instantiate(respawnablesPrefabs[i]);
                }
            }

            timer = 0;
            respawning = false;
        }
    }

    public void Add(Respawnable _obj)
    {
        respawnablesSceneObjects.Add(_obj);
    }

    public void Respawn()
    {
        respawning = true;
    }

    public void SetCheckpoint(Vector2 _pos, Quaternion _rot)
    {
        LastCheckpointPos = _pos;
        LastCheckpointRot = _rot;
        curCheckPointTime = GameTime;
        onCheckPointEvents?.Invoke();
    }

    public void ResetCheckpoint()
    {
        GameTime = curCheckPointTime;
        onResetEvents?.Invoke();
    }
}
