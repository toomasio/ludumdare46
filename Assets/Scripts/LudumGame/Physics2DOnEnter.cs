﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NgnUtilities;
using NgnPhysics;
using UnityEngine.Events;

public class Physics2DOnEnter : MonoBehaviour
{
    [SerializeField, ExpandableObject] private PhysicsDetectable2DData detectData;
    [SerializeField] private UnityEvent onEnterEvents;

    private IPhysicsDetectable2D system;

    protected virtual void Awake()
    {
        system = detectData.CreateSystemInstance(transform);
    }

    protected virtual void OnEnable()
    {
        system.OnEnter += OnEnter;
    }

    protected virtual void OnDisable()
    {
        system.OnEnter -= OnEnter;
    }

    protected virtual void FixedUpdate()
    {
        if (system == null) return;
        system.Tick();
    }

    protected virtual void OnEnter(Collider2D col)
    {
        onEnterEvents?.Invoke();
    }

    protected virtual void OnDrawGizmos()
    {
        if (detectData == null) return;
        if (system == null) system = detectData.CreateSystemInstance(transform);

        system.DrawDetectable();
    }
}
