﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NgnPhysics;
using NgnUtilities;

public class ItemSpawner : Item
{
    [SerializeField, ExpandableObject] protected SphereCast2DData rayData;
    [SerializeField] protected Transform rayOrigin;
    [SerializeField] protected Vector2 direction;
    [SerializeField] protected GameObject prefabToSpawn;
    [SerializeField] protected GameObject prefabPreview;


    protected GameObject curPreview;
    protected IPhysicsDetectable2D raySystem;

    protected virtual void OnEnable()
    {
        raySystem = rayData.CreateSystemInstance(rayOrigin);
    }

    protected virtual void OnDisable()
    {
        if (curPreview)
            Destroy(curPreview);
    }

    protected virtual void FixedUpdate()
    {
        TickRay();
        ShowSpawnPreview();
    }

    void TickRay()
    {
        if (direction != default)
            raySystem.Direction = direction;
        raySystem.Tick();
    }

    void ShowSpawnPreview()
    {
        if (raySystem == null) return;
        if (raySystem.Detected)
        {
            if (!curPreview)
            {
                curPreview = Instantiate(prefabPreview, raySystem.HitPosition, Quaternion.identity);
            }

            var rot = Quaternion.FromToRotation(Vector3.up, raySystem.HitNormal);
            var eul = rot.eulerAngles;
            var desRot = Quaternion.Euler(eul.x, 0, eul.z);

            curPreview.transform.position = raySystem.HitPosition;
            curPreview.transform.rotation = desRot;
        }
        else if (curPreview)
        {
            Destroy(curPreview);
        }
            
    }

    public override void Use()
    {
        SpawnPrefab();
    }

    protected virtual void SpawnPrefab()
    {
        if (!raySystem.Detected) return;
        var rot = curPreview != null ? curPreview.transform.rotation : Quaternion.identity;
        Instantiate(prefabToSpawn, raySystem.HitPosition, rot);
    }

    protected void OnDrawGizmos()
    {
        if (rayData == null || rayOrigin == null) return;
        if (raySystem == null) raySystem = rayData.CreateSystemInstance(rayOrigin);

        if (!Application.isPlaying) TickRay();
        raySystem.DrawDetectable();
    }
}
